#!/bin/bash -x

FLASH_VER_WIN=`curl -L http://fpdownload2.macromedia.com/get/flashplayer/update/current/xml/version_en_win_pl.xml 2>/dev/null | sed -n 's/.*update version="\([^"]*\).*/\1/p' | sed 's/,/./g'` 

FLASH_NUM=`echo "${FLASH_VER_WIN}" | awk -F'.' '{print $1}'`

if [ "x${FLASH_VER_WIN}" != "x" ]; then
    echo "Flash Version: ${FLASH_VER_WIN}"
    echo "${FLASH_VER_WIN}" > current-win-ver	
fi

# Update to handle distributed builds
if cmp current-win-ver old-win-ver; then
    # Don't build
    echo "Version unchanged"
else
    # Update win state
    cp current-win-ver old-win-ver
    
    # Let windows team know
	curl -d '{"color": "purple", "message": "Updated Flash Links:  https://fpdownload.macromedia.com/get/flashplayer/pdc/'"${FLASH_VER_WIN}"'/install_flash_player_'"${FLASH_NUM}"'_active_x.msi  &  https://fpdownload.macromedia.com/get/flashplayer/pdc/'"${FLASH_VER_WIN}"'/install_flash_player_'"${FLASH_NUM}"'_plugin.msi   ", "notify": false, "message_format": "text"}' -H 'Content-Type: application/json' https://miworkspace.hipchat.com/v2/room/2795973/notification?auth_token=RvOggsc513Mz5J2avM83Avw5JMvGIRTqZ6OUgmKV
   
fi

exit 0