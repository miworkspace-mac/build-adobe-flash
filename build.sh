#!/bin/bash -ex

# CONFIG
prefix="AdobeFlashPlayer"
suffix=""
munki_package_name="AdobeFlashPlayer"
display_name="Adobe Flash Plug-In"
icon_name=""
category="System Morsel"
description="This update contains stability and security fixes for the Adobe Flash Player Plug-in"
#url="https://fpdownload.macromedia.com/get/flashplayer/pdc/24.0.0.194/install_flash_player_osx.dmg"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36' "${url}"

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
cp "$mountpoint/Install Adobe Flash Player.app/Contents/Resources/Adobe Flash Player.pkg" "`pwd`/AdobeFlashPlayer.pkg"
/usr/sbin/pkgutil --expand "AdobeFlashPlayer.pkg" pkg
mkdir build-root
(cd build-root; pax -rz -f ../pkg/AdobeFlashPlayerComponent.pkg/Payload)
hdiutil detach "${mountpoint}"

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root AdobeFlashPlayer.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv AdobeFlashPlayer.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
